# Ognjen Cvetković
### hacker `::` developer `::` researcher
---
For me, being hacker means you are able to see how everything works, thus giving you the power to interact with each working element. But everyone knows that `with great power, comes great responsibility`.

Where the "non-hacker" sees the kitchen sink, hacker sees the whole network of interconnected working elements from manual vent switch for controlling the balance of hot and cold water, to a diagram of electrical components in your automatic waste disposer, that is a hacker.

Started developing my skills at an early age, found joy in it, spent time ever since playing with everything that has a silicate oscillator in it and can remember at least 8 bits.

---
## Serbian - Mother Language `::` English - Advanced 

## Education
**self-education** `::` **Petnica Science Center - Applied Physics and Electronics** `::` Gimnazija Lazarevac - Društeno jezički smer `::` Tehnička Škola Kolubara - Elektrotehničar elektromotornih pogona  

## SOME OF MY PROJECTS
  - 2021 `::` **GAVRAN**
    `::` Electric motorcycle division (eFighter)
  - 2021 `::` **LEPTIR**
    `::` Micro-drone AI coordinated swarm platform with focus on indoor/outdoor security and agronomy monitoring and research
  - 2020 `::` **KOIN**
    `::` KOIN Citizens Association, registered to promote digital tokens and innovative methods for storing and transferring money, using the KOIN platform, enables trade and storage of the KOIN digital token (KNX) which allows users to instantly receive and send funds anywhere in the world, free of charge
  - 2019 `::` **UniDrive**
    `::` "Uber-like" platform for innovative road assistance and ride/car sharing services
  - 2019 `::` **Appollon**
    `::` "Uber-like" platform for experts in the fields of fitness, sport, nutrition and medicine and "AI driven" platform for users to track their health and reach goals with the help of platform and experts
  - 2019 `::` **TheCannon**
    `::` AR mobile game similar to old PC game "Worms", where user use his mobile device as "cannon" for targeting and shooting their playmates accross the world  
  - 2019 `::` **RapidRabbit**
    `::` All-in-one workflow for collaborative, continuous, automated integration and deployment - global environments - test, build, deploy, update with a single click
  - 2018 `::` **BlackBulb**
    `::` AI and blockchain powered multiplatform for immigration tracking, terrorism pre-detection
  - 2018 `::` **JobStop.at**
    `::` Platform for posting and finding jobs. User have mobile application that will detect companies offering jobs on their location while they pass by them  
  - 2017 `::` **1+1+**
    `::` Cryptocurrency based crowdfunding platform
  - 2015 `::` **OM Security Platform**
    :: Early detection and prevention of malicious actions on PC
  - 2013 `::` **car-box**
    :: Opensource database of electro and car parts and components
  - 2012 `::` **WhiteCircle**
    `::` Online platform where businesses and independent information security professionals connect and collaborate remotely 
  - 2011 `::` **ATOM D21**
    `::` Autonomous flying drone platform, electro powered. Monitoring, search, ground support missions
  - 2009 `::` **iEYE**
    `::` Early detection and prevention of malicious actions on web servers
  - 2007 `::` **USB0**
    `::` All in one field information security pendrive
  - 2006 `::` **ROXi**
    `::` Windows based proxy web browser
  - 2005 `::` **PERSONAL PARTNER**
    `::` All in one, multiplatform ERP and CRM solution
  - 2004 `::` **bad_ip**
    `::` Modular database of malicious IP addresses
  - 2004 `::` **SMARTi PANEL**
    `::` Digital interactive multimedia info panel


## SOME OF THE PROJECTS I HAVE WORKED ON
  - **TicketMaster**
    `::` Android `::` Maintenance `::` Features implementation
  - **CBD American Shaman**
    `::` Laravel/Vue `::` Backend `::` Web development `::` DevOps
  - **AskBrokers**
    `::` PHP/Symfony `::` Backend `::` Web development `::` Team L
  - **Fashion&Friends**
    `::` Magento 2 Enterprise `::`  Backend `::` Microservices `::` DevOps
  - **Dr. Max**
    `::` PHP `::` Web Development `::` Team Lead `::` DevOps

  **AnacortesGunShop** `::` **Astromart** `::` **Photomarathon** `::` **Hatteras** `::` **AskGamblers Awards** `::` **Celldom** `::` **Phosfan** `::` **Hillout** `::` **BlackJack** `::` **RSJP** `::` **Plivacki Savez Srbije** `::` **WPSeed** `::` **MinMVC** `::` **Whisperr** `::` **SoGe BranchSelector** `::` 




## SOME OF MY SKILLS


### Software Development `:::` Microelectronics `:::` Information Security
---
### Industry Knowledge
**Penetration Testing** `::` **Computer Forensics** `::` **Automation** `::` **Big Data** `::` **Web Development** `::` **Kanban** `::` **Agile Methodologies** `::` **Integration** `::` **Android Development** `::` **Auditing**

---
### Tools & Technologies
**SQL** `::` **PHP** `::` **Python** `::` **C#** `::` **Ruby on Rails** `::` **Java** `::` **JavaScript** `::` **Linux** `::` **Windows** `::` **Content Management** **Systems (CMS)** `::` **Embedded Systems** `::` **Apache** `::` **Powershell** `::` **C++** `::` **Visual Basic .NET (VB.NET)** `::` **Arduino** `::` **Docker** `::` **Blockchain** `::` **VBScript** `::` **Go**

---
### Other Skills 


**DevOps** `::` **Digital Forensics** `::` **Mobile Forensics** `::` **Microchip PIC** `::` **Plugins** `::` **Microservices** `::` **FinTech** `::` **Networking** `::` **Back-End Web Development** `::` **Mobile** `::` **Application Architecture Development** `::` **Desktop Application Development** `::` **VB5/6** `::` **Symfony**

---
### Interpersonal Skills
**Team Leadership**



---
## ![alt text](https://cdnjs.cloudflare.com/ajax/libs/topcoat-icons/0.1.0/svg/like.svg "moto") family `::` nature `::` batcave
[LinkedIn](https://www.linkedin.com/in/unknown-id/)
